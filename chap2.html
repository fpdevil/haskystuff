<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="./css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./css/syntax.css">
    </head>
    <body>
        <div class="container">
            <h1 id="cgi-programming">CGI programming</h1>
<p>In this chapter, I’ll introduce you to programming CGI scripts in Haskell. We’ll naturally uncover more Haskell features while doing this and advance our understanding of the language.</p>
<p>But what is a CGI script? Back in the day, CGI, or Common Gateway Interface, would be used to run scripts through a web server. It could be Perl, Bash, or actually even an application written in C.</p>
<p>The behavior of a CGI script is somewhat similar to the behavior of a PHP script if you’re familiar with that language.</p>
<p>In order to run CGI scripts, you will need to configure a web server to run CGI scripts. We will use Apache for this example, but if you want you can use another server as well.</p>
<h2 id="configuring-apache-to-run-cgi-scripts">Configuring Apache to run CGI scripts</h2>
<p>Depending on your Apache setup, you can either add a default CGI directory in your httpd.conf, or in one of the other config files.</p>
<p>Add the following to your apache configuration:</p>
<pre><code>ScriptAlias /cgi-bin/ /path/to/your/cgi/files</code></pre>
<p>This will map the URL <code>yourdomain.com/cgi-bin/</code> to <code>/path/to/your/cgi/files</code>. Any CGI programs you put into that directory can then be executed by the web server. You should put the compiled files from this chapter into the cgi directory you configure for your server.</p>
<h2 id="making-a-simple-cgi-application">Making a simple CGI application</h2>
<p>The reason we’re using CGI and not something more advanced is because it lets us keep things relatively simple, and thus we can keep it easy to learn.</p>
<p>Let’s start by writing a CGI hello world: (Filename cgi1.hs)</p>
<pre><code>import Network.CGI

cgiMain :: CGI CGIResult
cgiMain = output &quot;Hello World&quot;

main :: IO ()
main = runCGI cgiMain</code></pre>
<p>This should look pretty familiar by now, but there’s again some new things here, primarily the <code>import</code> statement on top of the file.</p>
<p><code>import</code> lets us import other Haskell modules into our file. In this case, we are importing the <code>Network.CGI</code> module, which contains a CGI interface we use to communicate with the web server.</p>
<p>The other new things we can see is the <code>CGI CGIResult</code> type, and the functions we imported from <code>Network.CGI</code>.</p>
<p><code>CGI CGIResult</code> is similar to <code>IO a</code> that we saw earlier - it’s a computation that represents a CGI action, with a result of type <code>CGIResult</code> - in other words, this is <em>the CGI monad</em>. <code>CGIResult</code> is simply a data type used to represent the result - for example, returning some HTTP headers and the body “Hello World”, or in some other cases it could be a redirect and so forth.</p>
<p><code>output</code> is a function from <code>Network.CGI</code> that is used to output <code>String</code>s. Let’s use ghci again to try and understand the functions.</p>
<p>In order to have the functions in ghci, we must load them using <code>import</code>:</p>
<pre><code>Prelude&gt; import Network.CGI
Prelude Network.CGI&gt; :t output
output :: MonadCGI m =&gt; String -&gt; m CGIResult</code></pre>
<p>Let’s break this function’s type down so we understand what’s going on.</p>
<p>First, we have a type constraint for <code>MonadCGI m</code>. This means that <code>m</code> must implement the <code>MonadCGI</code> type class. The <code>CGI</code> monad we saw in the code is an implementation of it, so we know that we can use this function if our function’s return type is <code>CGI</code>. The reason it just doesn’t force the <code>CGI</code> monad directly is that there are ways to implement a custom <code>MonadCGI</code> to have additional features, so this allows <code>output</code> to work in other implementations as well. <code>CGI</code> is simply the default implementation.</p>
<p>Next, we have the type itself: <code>String -&gt; m CGIResult</code>, which means <code>output</code> takes a <code>String</code> as its argument, and returns something of type <code>m CGIResult</code>.</p>
<p>Typically when a type variable represents a monad, the letter <code>m</code> is used as its name. The type <code>m CGIResult</code> simply means that the monad’s result must be a <code>CGIResult</code>. Since we saw the type constraint, we also know the monad must implement <code>MonadCGI</code>.</p>
<p>Finally, we have <code>runCGI</code>.</p>
<pre><code>Prelude Network.CGI&gt; :t runCGI
runCGI :: MonadIO m =&gt; CGIT m CGIResult -&gt; m ()</code></pre>
<p>What’s all this now? More monads? Let’s break it down once more:</p>
<p><code>MonadIO m</code> is constraining the type variable <code>m</code> into those that implement <code>MonadIO</code>. You might already guess that the <code>IO</code> monad is an instance of <code>MonadIO</code>, and you would be right. So, <code>m</code> must be an <code>IO</code> monad.</p>
<p>The type for the parameter is <code>CGIT m CGIResult</code>. <code>CGIT</code> is something called a <em>monad transformer</em>. Typically their names end in an upper-case T, such as in this case. What a monad transformer does, it allows you to stack monads. In fact, if we look at the <code>CGI</code> monad, we’ll see it’s actually a <code>CGIT</code> transformer with <code>IO</code>.</p>
<pre><code>type CGI a = CGIT IO a</code></pre>
<p>So, now if we look at our <code>cgiMain</code> function…</p>
<pre><code>cgiMain :: CGI CGIResult</code></pre>
<p>This is the same as saying…</p>
<pre><code>cgiMain :: CGIT IO CGIResult</code></pre>
<p>Now you should be able to see that <code>runCGI</code> will accept <code>cgiMain</code> as its parameter because it has the correct type. Now, the final thing is… what is the return value?</p>
<pre><code>runCGI :: MonadIO m =&gt; CGIT m CGIResult -&gt; m ()</code></pre>
<p>If we look at how <code>CGI</code> was defined, we can see that the first parameter for <code>CGIT</code> was <code>IO</code>. So, in this case, <code>m</code> would be <code>IO</code>, and as such, the return value is <code>IO ()</code>, which matches <code>main</code> as expected.</p>
<h2 id="displaying-something-useful">Displaying something useful</h2>
<p>Let’s display the current date on our CGI page.</p>
<p>We can get the current time using a function called <code>getCurrentTime</code>. It resides in the <code>Data.Time</code> module.</p>
<p>Let’s try it out in ghci:</p>
<pre><code>Prelude&gt; import Data.Time
Prelude Data.Time&gt; :t getCurrentTime
getCurrentTime :: IO UTCTime</code></pre>
<p>Looks like this function uses the <code>IO</code> monad and returns an <code>UTCTime</code>. The side-effect of this function is reading the computer’s clock, so this is why it requires the <code>IO</code> monad.</p>
<pre><code>Prelude Data.Time&gt; getCurrentTime
2012-12-11 23:28:48.302951 UTC</code></pre>
<p>Now let’s write a CGI script to output this to a browser.</p>
<p>File: <a href="src/examples/cgi2.hs">cgi2.hs</a></p>
<pre class="sourceCode haskell"><code class="sourceCode haskell"><span class="kw">import</span> <span class="dt">Network.CGI</span>
<span class="kw">import</span> <span class="dt">Data.Time</span>

<span class="ot">cgiMain ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
cgiMain <span class="fu">=</span> <span class="kw">do</span>
    time <span class="ot">&lt;-</span> liftIO getCurrentTime
    output (<span class="fu">show</span> time)

<span class="ot">main ::</span> <span class="dt">IO</span> ()
main <span class="fu">=</span> runCGI cgiMain
</code></pre>
<p>It looks pretty similar to the earlier examples. We import the necessary modules, have a <code>cgiMain</code> which runs our code, and the <code>main</code> function simply runs <code>cgiMain</code> in the <code>CGI</code> monad using <code>runCGI</code>.</p>
<p>But what’s <code>liftIO</code>, and why is it used with <code>getCurrentTime</code>?</p>
<p>You may remember the type of <code>getCurrentTime</code> is <code>IO UTCTime</code>, and the type of <code>cgiMain</code> is <code>CGI CGIResult</code>. We cannot run <code>IO</code> functions in the <code>CGI</code> monad, so we must “lift” the function from <code>IO</code> into <code>CGI</code>.</p>
<p>Recall when I said that monad transformers allow you to stack monads on top of each other. The <code>CGI</code> monad is actually a transformer <code>CGIT IO</code>. So essentially we stack the <code>CGI</code> functionality on top of the <code>IO</code> monad, and in order to make our <code>IO</code> function work in the <code>CGI</code> monad, we must lift it up the stack using <code>liftIO</code>.</p>
<h2 id="getting-user-inputs">Getting user inputs</h2>
<p>Naturally we must also be able to process user inputs in a proper web application.</p>
<p>We can use the function <code>getInput</code> to read inputs from the query string or from the post body.</p>
<pre><code>getInput :: MonadCGI m =&gt; String -&gt; m (Maybe String)</code></pre>
<p>Let’s use this function to create a small application that shows different pages depending on user input, as is often common:</p>
<p>File: <a href="src/examples/cgi3.hs">cgi3.hs</a></p>
<pre class="sourceCode haskell"><code class="sourceCode haskell"><span class="kw">import</span> <span class="dt">Network.CGI</span>

<span class="ot">main ::</span> <span class="dt">IO</span> ()
main <span class="fu">=</span> runCGI cgiMain

<span class="ot">cgiMain ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
cgiMain <span class="fu">=</span> <span class="kw">do</span>
    action <span class="ot">&lt;-</span> getInput <span class="st">&quot;page&quot;</span>
    processAction action

<span class="ot">processAction ::</span> <span class="dt">Maybe</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
processAction (<span class="kw">Just</span> <span class="st">&quot;foo&quot;</span>) <span class="fu">=</span> output <span class="fu">$</span> <span class="st">&quot;&lt;p&gt;Foo page&lt;/p&gt;&quot;</span> <span class="fu">++</span> links
processAction (<span class="kw">Just</span> <span class="st">&quot;bar&quot;</span>) <span class="fu">=</span> output <span class="fu">$</span> <span class="st">&quot;&lt;p&gt;Bar page&lt;/p&gt;&quot;</span> <span class="fu">++</span> links
processAction _ <span class="fu">=</span> output <span class="fu">$</span> <span class="st">&quot;&lt;p&gt;Home page&lt;/p&gt;&quot;</span> <span class="fu">++</span> links

<span class="ot">links ::</span> <span class="dt">String</span>
links <span class="fu">=</span> <span class="fu">unlines</span>
        [ <span class="st">&quot;&lt;p&gt;&lt;a href=\&quot;?page=foo\&quot;&gt;Go to Page 2&lt;/a&gt;&lt;/p&gt;&quot;</span>
        , <span class="st">&quot;&lt;p&gt;&lt;a href=\&quot;?page=bar\&quot;&gt;Go to Page 3&lt;/a&gt;&lt;/p&gt;&quot;</span> 
        ]</code></pre>
<p>If we run this program via the web server, we get presented with some text and two links. Clicking each of the links takes us to another page with another message.</p>
<p>The application should look mostly familiar with a few new functions, and the peculiar looking <code>$</code> syntax. The <code>$</code> is infact just another function. Let’s go over the source.</p>
<p>As previously, we have a <code>main</code> function which runs our <code>cgiMain</code> function. We could actually call <code>cgiMain</code> something else too - it doesn’t have to be called <code>cgiMain</code>.</p>
<p>In this application, <code>cgiMain</code> uses <code>getInput</code> to read a parameter called <em>page</em>. This can be either in the query string, or in the POST body. Once it has read the parameter, it then calls the function <code>processAction</code> with the value as its parameter.</p>
<p>Since <code>getInput</code> returns a <code>Maybe</code>, the <code>processAction</code> function must also take a <code>Maybe</code>. In this case, we use simple pattern matches to direct what happens: If the <code>Maybe</code> contains a value - in other words, a <code>Just</code> - we either match against <code>Just &quot;foo&quot;</code> or <code>Just &quot;bar&quot;</code>. The last pattern match matches any value - it will match a <code>Nothing</code>, or a <code>Just</code> that contains some other string besides <code>&quot;foo&quot;</code> or <code>&quot;bar&quot;</code>.</p>
<p>Essentially what this does, is it displays the home page message for any page value except foo and bar.</p>
<p>But what does <code>$</code> do?</p>
<pre><code>Prelude&gt; :t ($)
($) :: (a -&gt; b) -&gt; a -&gt; b</code></pre>
<p>When you see a value in parentheses in a signature like this, it is a function. So <code>(a -&gt; b)</code> means the first parameter must be a function with a signature <code>a -&gt; b</code> - a function that takes a single argument of some type <code>a</code>, and returns a value of some type <code>b</code>.</p>
<p>The second parameter looks is just a value of <code>a</code> and it returns a <code>b</code>.</p>
<p><code>$</code> is called the function application function. It has a <em>right fixity</em> - that means it evaluates the value on its right side first, and only after that, the value on the left side.</p>
<p>Confused? Don’t worry. It’s actually very simple. Compare these two lines:</p>
<pre><code>output $ &quot;&lt;p&gt;Foo page&lt;/p&gt;&quot; ++ links
output (&quot;&lt;p&gt;Foo page&lt;/p&gt;&quot; ++ links)</code></pre>
<p>They both do exactly the same thing. So in other words, you can use <code>$</code> to replace using parentheses, which can result in code that’s easier to read. Here’s another comparison:</p>
<pre><code>aFunc (fooFunc (barFunc someValue))
aFunc $ fooFunc $ barFunc someValue</code></pre>
<p>Again, these are equivalent. Even if you don’t fully understand the type signature or fixity, the important thing to remember about <code>$</code> is how it’s used - in place of parentheses.</p>
<p><code>$</code> is commonly used in Haskell code, so even if it looks a bit weird at first, it’s good to know its usage and you’ll get used to it soon.</p>
<p>Continuing with the code, we finally have the function <code>links</code>. The sole purpose of this function is to output a <code>String</code> containing the HTML for the links, so we don’t need to repeat the markup all over the place.</p>
<p>We use the function <code>unlines</code> to convert a list of <code>String</code>s into a single <code>String</code>. <code>unlines</code> simply joins the list into a single value by placing a newline between each list item.</p>
<pre><code>Prelude&gt; unlines [&quot;foo&quot;, &quot;bar&quot;]
&quot;foo\nbar\n&quot;</code></pre>
<p>The list in the <code>links</code> function may seem a bit oddly formatted. It’s still the parameter to <code>unlines</code> even though it’s on the next line because we indented it. This style is sometimes used in Haskell lists to make sure you can easily spot missing commas or such.</p>
<h2 id="reading-and-writing-files">Reading and writing files</h2>
<p>What would we do without being able to store data somewhere?</p>
<p>Let’s learn about file I/O by writing a CGI application that logs the visitor’s IP addresses. We can then move on and combine these into a bit larger application to test our might!</p>
<p>In order to read the CGI user’s IP address, we can use the function <code>getVar</code></p>
<pre><code>getVar :: MonadCGI m =&gt; String -&gt; m (Maybe String)</code></pre>
<p>A pretty standard looking CGI function. Now, to read and write files, we can use <code>readFile</code> and <code>writeFile</code>.</p>
<pre><code>readFile :: FilePath -&gt; IO String
writeFile :: FilePath -&gt; String -&gt; IO ()</code></pre>
<p>They both use the <code>IO</code> monad due to the side-effect of touching files. The <code>FilePath</code> type is just a synonym for <code>String</code>, but this is a good example where using a synonym makes it much easier to understand the signature.</p>
<p>However, <code>writeFile</code> overwrites the file. We only want to append the latest IP into the bottom of the list. For this, we need to use some slightly more lower level file functions:</p>
<pre><code>openFile :: FilePath -&gt; IOMode -&gt; IO Handle
hPutStrLn :: Handle -&gt; String -&gt; IO ()
hClose :: Handle -&gt; IO ()</code></pre>
<p>These functions operate on <em>file handles</em>. You may be familiar with file pointers or file handles from other languages, and they behave essentially the same in Haskell.</p>
<p><code>openFile</code> opens a file for <code>IOMode</code> - <code>IOMode</code> is a data type used to indicate whether we want to read, write or append. <code>hPutStrLn</code> writes a string + a newline into a handle, and <code>hClose</code> is used to close handles. Note that these two will work on other types of handles too, such as network sockets.</p>
<p>Let’s have a go:</p>
<p>File: <a href="src/examples/cgi4.hs">cgi4.hs</a></p>
<pre class="sourceCode haskell"><code class="sourceCode haskell"><span class="kw">import</span> <span class="dt">Network.CGI</span>
<span class="kw">import</span> <span class="dt">System.IO</span>

<span class="ot">main ::</span> <span class="dt">IO</span> ()
main <span class="fu">=</span> runCGI cgiMain

<span class="ot">writeIP ::</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">IO</span> ()
writeIP ip <span class="fu">=</span> <span class="kw">do</span>
    handle <span class="ot">&lt;-</span> openFile <span class="st">&quot;ipaddrs.log&quot;</span> <span class="dt">AppendMode</span>
    hPutStrLn handle ip
    hClose handle

<span class="ot">cgiMain ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
cgiMain <span class="fu">=</span> <span class="kw">do</span>
    mAddr <span class="ot">&lt;-</span> getVar <span class="st">&quot;REMOTE_ADDR&quot;</span>
    <span class="kw">case</span> mAddr <span class="kw">of</span>
      <span class="kw">Nothing</span> <span class="ot">-&gt;</span> <span class="kw">do</span>
          liftIO <span class="fu">$</span> writeIP <span class="st">&quot;no IP&quot;</span>
          showLog
      <span class="kw">Just</span> ip <span class="ot">-&gt;</span> <span class="kw">do</span>
          liftIO <span class="fu">$</span> writeIP ip
          showLog

<span class="ot">showLog ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
showLog <span class="fu">=</span> <span class="kw">do</span>
    addresses <span class="ot">&lt;-</span> liftIO <span class="fu">$</span> <span class="fu">readFile</span> <span class="st">&quot;ipaddrs.log&quot;</span>
    output addresses</code></pre>
<p>Note: In order for this application to work, you may need to change the ownership or permissions of the final CGI binary. In my case on Ubuntu, I needed to <code>chown www-data cgi4</code> for it to be able to write the log file. If you have an error, you can find it in your Apache error log.</p>
<p>In this case we didn’t have much new stuff. Mainly the part where we write into a file.</p>
<p>Let’s have a quick look at <code>writeIP</code> and <code>showLog</code>:</p>
<p>Since we are using functions that use the <code>IO</code> monad in <code>writeIP</code>, we need to make its type <code>IO</code> as well. The code in it should be easy to understand: We open a file handle, write the IP address into it and finally close the handle.</p>
<p>Now, in <code>cgiMain</code> when using <code>writeIP</code>, we must remember to use <code>liftIO</code>. Recall that the <code>CGI</code> monad contains the <code>IO</code> monad, and we cannot run the <code>IO</code> function in <code>CGI</code> unless we lift it to <code>IO</code>.</p>
<p>In <code>showLog</code> we also access the file, but in this case we simply use <code>readFile</code> to read the entire file in one go. Since we made the type of <code>showLog</code> be <code>CGI CGIResult</code>, we again must use <code>liftIO</code> for <code>readFile</code> to work.</p>
<p>But why did we make <code>showLog</code> use <code>CGI</code> instead of <code>IO</code>, like <code>writeIP</code>?</p>
<p>The answer is the <code>output</code> function in the end: <code>output</code> uses <code>CGI</code>, so in order for us to be able to use it, we must be in the <code>CGI</code> monad. We can’t “unlift”, so we must use <code>CGI</code> in the function, and then use <code>liftIO</code> for <code>readFile</code>.</p>
<p>Even if lifting and such may seem a bit tricky, you’ll start to understand it with practice. For now, just keep in mind that in order to use functions in <code>IO</code>, they must be lifted or the function that calls them must be in <code>IO</code> itself. There are other cases where lifting can be used, but these are by far the most common.</p>
<h3 id="improving-our-logging-application">Improving our logging application</h3>
<p>There is however a small improvement we could easily make to the application. The <code>cgiMain</code> function looks a bit convoluted with the whole case-lift-showlog mess.</p>
<p>We can see that it’s caused by the <code>Maybe</code>: We need to check if the IP address was given or not in order to log the correct message if there is no IP.</p>
<p>Thankfully, we can easily avoid the pattern matching - There’s a function called <code>getVarWithDefault</code> in CGI:</p>
<pre><code>getVarWithDefault :: MonadCGI m =&gt; String -&gt; String -&gt; m String</code></pre>
<p>Instead of returning <code>Nothing</code> in a case where the value is not found, we can give this function a default. Let’s change our <code>cgiMain</code> function:</p>
<p>File: <a href="src/examples/cgi4b.hs">cgi4b.hs</a></p>
<pre class="sourceCode haskell"><code class="sourceCode haskell"><span class="kw">import</span> <span class="dt">Network.CGI</span>
<span class="kw">import</span> <span class="dt">System.IO</span>

<span class="ot">main ::</span> <span class="dt">IO</span> ()
main <span class="fu">=</span> runCGI cgiMain

<span class="ot">writeIP ::</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">IO</span> ()
writeIP ip <span class="fu">=</span> <span class="kw">do</span>
    handle <span class="ot">&lt;-</span> openFile <span class="st">&quot;ipaddrs.log&quot;</span> <span class="dt">AppendMode</span>
    hPutStrLn handle ip
    hClose handle

<span class="ot">cgiMain ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
cgiMain <span class="fu">=</span> <span class="kw">do</span>
    addr <span class="ot">&lt;-</span> getVarWithDefault <span class="st">&quot;REMOTE_ADDR&quot;</span> <span class="st">&quot;no IP&quot;</span>
    liftIO <span class="fu">$</span> writeIP addr
    showLog

<span class="ot">showLog ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
showLog <span class="fu">=</span> <span class="kw">do</span>
    addresses <span class="ot">&lt;-</span> liftIO <span class="fu">$</span> <span class="fu">readFile</span> <span class="st">&quot;ipaddrs.log&quot;</span>
    output addresses</code></pre>
<p>Isn’t that a whole lot simpler? It pays to keep an eye on what functions are available in each of the module!</p>
<p>In a later chapter, I’ll show you how to find out more about what functions each module provides and other useful tips.</p>
<h2 id="putting-it-together">Putting it together</h2>
<p>Let’s find out how mighty our Haskell is by writing a bit larger application.</p>
<p>This one will allow you to leave messages, sort of like a simple guestbook.</p>
<p>File: <a href="src/examples/cgi5.hs">cgi5.hs</a></p>
<pre class="sourceCode haskell"><code class="sourceCode haskell"><span class="kw">import</span> <span class="dt">Network.CGI</span>
<span class="kw">import</span> <span class="dt">System.Directory</span>
<span class="kw">import</span> <span class="dt">System.IO</span>

<span class="ot">cgiMain ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
cgiMain <span class="fu">=</span> <span class="kw">do</span>
    action <span class="ot">&lt;-</span> getInput <span class="st">&quot;action&quot;</span>
    processAction action

<span class="ot">main ::</span> <span class="dt">IO</span> ()
main <span class="fu">=</span> <span class="kw">do</span>
    initMessagesFile
    runCGI cgiMain

<span class="ot">initMessagesFile ::</span> <span class="dt">IO</span> ()
initMessagesFile <span class="fu">=</span> <span class="kw">do</span>
    messagesExists <span class="ot">&lt;-</span> doesFileExist <span class="st">&quot;messages.dat&quot;</span>
    <span class="kw">if</span> messagesExists <span class="fu">==</span> <span class="kw">False</span> 
        <span class="kw">then</span> <span class="fu">writeFile</span> <span class="st">&quot;messages.dat&quot;</span> <span class="st">&quot;&quot;</span>
        <span class="kw">else</span> <span class="fu">return</span> ()

<span class="ot">processAction ::</span> <span class="dt">Maybe</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
processAction (<span class="kw">Just</span> <span class="st">&quot;post&quot;</span>) <span class="fu">=</span> <span class="kw">do</span>
    mmessage <span class="ot">&lt;-</span> getInput <span class="st">&quot;message&quot;</span>
    <span class="kw">case</span> mmessage <span class="kw">of</span>
        <span class="kw">Nothing</span>      <span class="ot">-&gt;</span> output <span class="st">&quot;Form did not contain all required values&quot;</span>
        <span class="kw">Just</span> message <span class="ot">-&gt;</span> <span class="kw">do</span>
            liftIO <span class="fu">$</span> writeMessage message
            redirect <span class="st">&quot;cgi5&quot;</span>

processAction _ <span class="fu">=</span> <span class="kw">do</span>
    messages <span class="ot">&lt;-</span> liftIO readMessages
    output <span class="fu">$</span> (messagesHtml messages) <span class="fu">++</span> formHtml

<span class="ot">messagesHtml ::</span> [<span class="dt">String</span>] <span class="ot">-&gt;</span> <span class="dt">String</span>
messagesHtml ms <span class="fu">=</span> <span class="fu">concat</span> <span class="fu">$</span> <span class="fu">map</span> messageHtml ms
    <span class="kw">where</span>
        messageHtml message <span class="fu">=</span> <span class="st">&quot;&lt;p&gt;&quot;</span> <span class="fu">++</span> message <span class="fu">++</span> <span class="st">&quot;&lt;/p&gt;&quot;</span>

<span class="ot">formHtml ::</span> <span class="dt">String</span>
formHtml <span class="fu">=</span> <span class="fu">unlines</span>
            [ <span class="st">&quot;&lt;form action=?action=post method=post&gt;&quot;</span>
            , <span class="st">&quot;&lt;input type=text name=message&gt;&quot;</span>
            , <span class="st">&quot;&lt;input type=submit value=Send&gt;&quot;</span>
            ]

<span class="ot">readMessages ::</span> <span class="dt">IO</span> [<span class="dt">String</span>]
readMessages <span class="fu">=</span> <span class="kw">do</span>
    raw <span class="ot">&lt;-</span> <span class="fu">readFile</span> <span class="st">&quot;messages.dat&quot;</span>
    <span class="fu">return</span> <span class="fu">$</span> <span class="fu">lines</span> raw

<span class="ot">writeMessage ::</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">IO</span> ()
writeMessage m <span class="fu">=</span> <span class="kw">do</span>
    handle <span class="ot">&lt;-</span> openFile <span class="st">&quot;messages.dat&quot;</span> <span class="dt">AppendMode</span>
    hPutStrLn handle m
    hClose handle</code></pre>
<p>In this case, when visiting in the browser, the application displays a list of messages and a form to send your own. When sending a message, the form has some validation and if all goes well it redirects you back to the initial page.</p>
<p>Let’s go over each of the functions.</p>
<p><code>cgiMain</code> and <code>main</code> should be pretty standard. In <code>cgiMain</code> we just grab a parameter called “action” and then like in the earlier example, have another function handle it.</p>
<p>In <code>main</code> we have one additional function besides <code>runCGI</code>: <code>initMessagesFile</code></p>
<p>Because when a user comes on the page, we try to read the messages from the file, the file must exist. Thus, we use <code>initMessagesFile</code> to ensure the file exists before we try to use it. The function itself simply checks if the file exists, and if not, it writes an empty string to it.</p>
<p>In the <code>else</code> part of <code>initMessagesFile</code> there’s something that looks like a return-statement. However, do not be tricked, it isn’t. <code>return</code> is a function which puts a value into a monad such as <code>IO</code>.</p>
<pre><code>return :: Monad m =&gt; a -&gt; m a</code></pre>
<p>Remember that the last value produced by a function is its return value. <code>initMessagesFile</code>’s return type is <code>IO ()</code>, so in order for it to work, the last value must be of type <code>IO ()</code>. As you can see, we pass <code>()</code> as a parameter to return.</p>
<p>We can see that <code>return</code>’s value is <code>m a</code>. Since <code>m</code> is a type variable for any <code>Monad</code>, GHC is able to infer that <code>m</code> must be <code>IO</code> because that’s what our function is. Thus, the final value of <code>initMessagesFile</code> becomes <code>IO ()</code> like required.</p>
<p>But why does <code>writeFile</code> work without using <code>return</code>? If you recall, the type for <code>writeFile</code>:</p>
<pre><code>writeFile :: FilePath -&gt; String -&gt; IO ()</code></pre>
<p>We can see the value returned by it is already <code>IO ()</code>, which then becomes the final value produced by <code>initMessagesFile</code> as well.</p>
<p>Continuing with <code>processAction</code>, it’s quite similar to the earlier example. This time around, we only have the main page case and one “action” - “post”. When posting, we try to read a parameter called “message”. If the user did not provide a message, we use <code>output</code> to show an error. Otherwise we write the message into a file and redirect the user back to the main page.</p>
<p>The main page case for <code>processAction</code> reads all messages from the file and outputs the result of <code>messagesHtml</code> and <code>formHtml</code> concatenated together.</p>
<p><code>messagesHtml</code> is just a function which takes a list of <code>String</code>s returning another <code>String</code>. Here, we use one of the common list functions, <code>map</code>, to iterate over the list and calling a function on each of the items. <code>concat</code> is used to join together a list of lists into a single list, but since <code>String</code>s are also lists, it works on them too: Prelude&gt; concat [[1, 2], [3, 4]] [1,2,3,4] Prelude&gt; concat [“foo”, “bar”] “foobar”</p>
<p><code>messagesHtml</code> also contains some new syntax: <code>where</code></p>
<p><code>where</code> can be used to declare local variables or local functions which are immediately available in the main function body. In this case, we use it to declare a helper function <code>messageHtml</code> which we use in the <code>map</code> function.</p>
<p>Let’s take a moment to learn about <code>map</code>.</p>
<pre><code>map :: (a -&gt; b) -&gt; [a] -&gt; [b]</code></pre>
<p>You may remember from <code>$</code> that a value in parentheses in a signature means the value is a function. So, <code>map</code> takes a function as its first parameter, and a list of <code>a</code>s as the second. Finally, it returns a list of <code>b</code>s.</p>
<p>Since we know that <code>map</code> traverses a list, calling the function on each item, we can see the connection between the parameters and the return value: The first parameter must take a value of the same type as in the list, and return some type <code>b</code> which is then placed into a list and returned by <code>map</code>.</p>
<pre><code>Prelude&gt; let myFun a = a ++ &quot;bar&quot;
Prelude&gt; map myFun [&quot;foo&quot;, &quot;bar&quot;, &quot;baz&quot;]
[&quot;foobar&quot;,&quot;barbar&quot;,&quot;bazbar&quot;]</code></pre>
<p>This is basically what <code>messagesHtml</code> does. It takes a list of messages, and wraps them with some markup. Finally, the result is concatenated into a single <code>String</code></p>
<pre><code>Prelude&gt; let myFun a = a ++ &quot;bar&quot;
Prelude&gt; concat $ map myFun [&quot;foo&quot;, &quot;bar&quot;, &quot;baz&quot;]
&quot;foobarbarbarbazbar&quot;</code></pre>
<p>Continuing with going through the example, <code>formHtml</code> is a function that simply returns some markup to create a form. It’s similar to the earlier example which created markup for links.</p>
<p>The result of the messages and form are both <code>String</code>s, so they are concatenated and sent as the output of <code>processAction</code> for the main page, which results in a list of messages and a form being rendered in the browser.</p>
<p>Finally, we have <code>readMessages</code> and <code>writeMessage</code>.</p>
<p><code>readMessages</code> is used to read the list of messages from the file. Since we want to return each message as a separate item, we use <code>lines</code> to split a <code>String</code> separated with newlines into a list.</p>
<pre><code>Prelude&gt; lines &quot;foo\nbar\n\baz&quot;
[&quot;foo&quot;,&quot;bar&quot;,&quot;\baz&quot;]</code></pre>
<p><code>lines</code> is basically the opposite for <code>unlines</code>.</p>
<p>And again, you see the <code>return</code> function being used. Just like earlier, the return type is <code>IO [String]</code>, so we must put the list into the <code>IO</code> monad using <code>return</code>.</p>
<p>Lastly, <code>writeMessage</code> takes a <code>String</code> and writes it into a file. It works exactly the same as the earlier example with logging IP addresses.</p>
<h3 id="improving-the-message-application">Improving the message application</h3>
<p>We could write the message application much more concisely by fully utilizing Haskell’s features.</p>
<p>Some of this may look confusing, but I’ll show you how much shorter it could be:</p>
<p>File: <a href="src/examples/cgi5b.hs">cgi5b.hs</a></p>
<pre class="sourceCode haskell"><code class="sourceCode haskell"><span class="kw">import</span> <span class="dt">Network.CGI</span>
<span class="kw">import</span> <span class="dt">System.IO</span>

<span class="ot">cgiMain ::</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
cgiMain <span class="fu">=</span> getInput <span class="st">&quot;action&quot;</span> <span class="fu">&gt;&gt;=</span> processAction

<span class="ot">main ::</span> <span class="dt">IO</span> ()
main <span class="fu">=</span> initMessagesFile <span class="fu">&gt;&gt;</span> runCGI cgiMain

<span class="ot">initMessagesFile ::</span> <span class="dt">IO</span> ()
initMessagesFile <span class="fu">=</span> writeMessage <span class="st">&quot;&quot;</span>

<span class="ot">processAction ::</span> <span class="dt">Maybe</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">CGI</span> <span class="dt">CGIResult</span>
processAction (<span class="kw">Just</span> <span class="st">&quot;post&quot;</span>) <span class="fu">=</span> <span class="kw">do</span>
    mmessage <span class="ot">&lt;-</span> getInput <span class="st">&quot;message&quot;</span>
    <span class="kw">case</span> mmessage <span class="kw">of</span>
        <span class="kw">Nothing</span>      <span class="ot">-&gt;</span> output <span class="st">&quot;Form did not contain all required values&quot;</span>
        <span class="kw">Just</span> message <span class="ot">-&gt;</span> <span class="kw">do</span>
            liftIO <span class="fu">$</span> writeMessage message
            redirect <span class="st">&quot;cgi5&quot;</span>

processAction _ <span class="fu">=</span> <span class="kw">do</span>
    messages <span class="ot">&lt;-</span> liftIO readMessages <span class="fu">&gt;&gt;=</span> <span class="fu">return</span> <span class="fu">.</span> messagesHtml
    output <span class="fu">$</span> messages <span class="fu">++</span> formHtml

<span class="ot">messagesHtml ::</span> [<span class="dt">String</span>] <span class="ot">-&gt;</span> <span class="dt">String</span>
messagesHtml ms <span class="fu">=</span> <span class="fu">concatMap</span> (\m <span class="ot">-&gt;</span> <span class="st">&quot;&lt;p&gt;&quot;</span> <span class="fu">++</span> m <span class="fu">++</span> <span class="st">&quot;&lt;/p&gt;&quot;</span>)  ms

<span class="ot">formHtml ::</span> <span class="dt">String</span>
formHtml <span class="fu">=</span> <span class="fu">unlines</span>
            [ <span class="st">&quot;&lt;form action=?action=post method=post&gt;&quot;</span>
            , <span class="st">&quot;&lt;input type=text name=message&gt;&quot;</span>
            , <span class="st">&quot;&lt;input type=submit value=Send&gt;&quot;</span>
            ]

<span class="ot">readMessages ::</span> <span class="dt">IO</span> [<span class="dt">String</span>]
readMessages <span class="fu">=</span> <span class="fu">readFile</span> <span class="st">&quot;messages.dat&quot;</span> <span class="fu">&gt;&gt;=</span> <span class="fu">return</span> <span class="fu">.</span> <span class="fu">lines</span>

<span class="ot">writeMessage ::</span> <span class="dt">String</span> <span class="ot">-&gt;</span> <span class="dt">IO</span> ()
writeMessage m <span class="fu">=</span> withFile <span class="st">&quot;messages.dat&quot;</span> <span class="dt">AppendMode</span> <span class="fu">$</span> (\handle <span class="ot">-&gt;</span> hPutStrLn handle m)</code></pre>
<p>The total length of the program went from 56 lines to 41 lines for a reduction of about 27%. If we consider only the functions that we changed, the reduction is even larger: 68%!</p>
<p>We could obviously take it further, but it would just sacrifice readability for code-golfing. However, the changes I made to it now actually improve the code if you understand all the meanings.</p>
<p>In the next chapter, we will study the more advanced features of Haskell some more, so that we can return back to this and understand the changes done.</p>
        </div>
    </body>
</html>
