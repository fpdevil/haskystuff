import Network.Wai
import Network.Wai.Handler.Warp (run)
import Network.HTTP.Types (status200)
import Data.ByteString.Lazy.Char8 (pack)

main :: IO ()
main = run 8010 (\_ -> return $ responseLBS status200 [] (pack "Hi!"))
