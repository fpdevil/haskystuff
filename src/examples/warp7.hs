{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp
import Network.HTTP.Types
import Control.Monad.IO.Class
import Data.Text (Text, intercalate, dropWhileEnd)
import qualified Data.ByteString.Lazy.Char8 as BSL
import Text.StringTemplate
import Template
import TemplateBlaze
import Text.Blaze.Html.Renderer.Utf8
import Database.HDBC
import Database.HDBC.Sqlite3
import Data.Maybe
import Control.Monad.Reader

data WebappEnv = WebappEnv { envRoutes :: [Route]
                           , envConnection :: Connection
                           }

type Webapp = Request -> ReaderT WebappEnv IO Response

data Route = Route { routeUrl :: Text
                   , routeHandler :: Webapp
                   }

lookupHandler :: [Route] -> Text -> Maybe Webapp
lookupHandler routes path = lookup (dropWhileEnd (== '/') path) routePairs
    where
        routePairs = map (\(Route url handler) -> (url, handler)) routes

dispatch :: [Route] -> Webapp
dispatch routes req = case lookupHandler routes $ intercalate "/" $ pathInfo req of
                        Nothing  -> return $ responseLBS status404 [] "Not Found"
                        Just app -> app req

routes = [ Route "" homeHandler
         , Route "ip" ipHandler
         , Route "about" aboutHandler
         ]

homeHandler :: Webapp
homeHandler req = do
    conn <- ask >>= return . envConnection
    result <- liftIO $ quickQuery' conn "SELECT * FROM users" []
    -- Do something with result

    tpl <- liftIO $ loadTemplate "home.st"
    let html = render $ setAttribute "ip" (show $ remoteHost req) tpl
    return $ responseLBS status200 [] $ BSL.pack html

ipHandler :: Webapp
ipHandler req = do
    return $ responseLBS status200 [] $ renderHtml $ layout $ ipPage (remoteHost req)

aboutHandler :: Webapp
aboutHandler _ = do
    return $ responseLBS status200 [] $ renderHtml $ layout aboutPage

main :: IO ()
main = do
    c <- connectSqlite3 "test.db"
    initDB c
    let ctx = WebappEnv routes c
    Warp.run 8010 $ runWebapp ctx

runWebapp :: WebappEnv -> Application
runWebapp context req = liftIO $ runReaderT (dispatch (envRoutes context) req) context

initDB :: Connection -> IO ()
initDB conn = do
        tables <- quickQuery' conn "SELECT name FROM sqlite_master WHERE type = 'table'" []
        let tableNames = map getName tables
        case elem "users" tableNames of
          True -> return ()
          False -> do run conn ("CREATE TABLE users "
                            ++ "(id INT NOT NULL PRIMARY KEY,"
                            ++ "login VARCHAR(100) NOT NULL,"
                            ++ "password VARCHAR(100) NOT NULL)") []
                      return ()
    where 
        getName :: [SqlValue] -> String
        getName [name] = fromJust $ fromSql name
