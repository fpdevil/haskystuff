import Network.CGI
import System.Directory
import System.IO

cgiMain :: CGI CGIResult
cgiMain = do
    action <- getInput "action"
    processAction action

main :: IO ()
main = do
    initMessagesFile
    runCGI cgiMain

initMessagesFile :: IO ()
initMessagesFile = do
    messagesExists <- doesFileExist "messages.dat"
    if messagesExists == False 
        then writeFile "messages.dat" ""
        else return ()

processAction :: Maybe String -> CGI CGIResult
processAction (Just "post") = do
    mmessage <- getInput "message"
    case mmessage of
        Nothing      -> output "Form did not contain all required values"
        Just message -> do
            liftIO $ writeMessage message
            redirect "cgi5"

processAction _ = do
    messages <- liftIO readMessages
    output $ (messagesHtml messages) ++ formHtml

messagesHtml :: [String] -> String
messagesHtml ms = concat $ map messageHtml ms
    where
        messageHtml message = "<p>" ++ message ++ "</p>"

formHtml :: String
formHtml = unlines
            [ "<form action=?action=post method=post>"
            , "<input type=text name=message>"
            , "<input type=submit value=Send>"
            ]

readMessages :: IO [String]
readMessages = do
    raw <- readFile "messages.dat"
    return $ lines raw

writeMessage :: String -> IO ()
writeMessage m = do
    handle <- openFile "messages.dat" AppendMode
    hPutStrLn handle m
    hClose handle
