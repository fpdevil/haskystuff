module Template 
        ( loadTemplate
        , example
        , SomeDataType (ConstructorA, ConstructorB)
        , OtherDataType (..)
        , MyRecord (MyRecord, mra)
        )
        where

loadTemplate :: String -> String
loadTemplate str = undefined

example :: Int
example = 10

notExported :: a
notExported = undefined

data SomeDataType = ConstructorA | ConstructorB | ConstructorC

data OtherDataType = OtherA | OtherB

data MyRecord = MyRecord { mra :: String, mrb :: String }

