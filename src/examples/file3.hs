names :: [(String, String)]
names = [("Bob", "Marley"), ("John", "Smith"), ("William", "Murderface")]

main :: IO ()
main = do
    firstName <- getLine
    case lookup firstName names of
        Just lastName -> putStrLn ("Last name: " ++ lastName)
        Nothing       -> putStrLn "Not found"
