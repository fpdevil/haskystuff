type Username = String
type Password = String

data User = User Username Password

userName :: User -> Username
userName (User n _) = n

userPassword :: User -> Password
userPassword (User _ p) = p

main :: IO ()
main = do
    let u = User "test" "pass"
    putStrLn (userName u)
    putStrLn (userPassword u)
