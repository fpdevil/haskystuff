import Network.CGI
import System.IO

main :: IO ()
main = runCGI cgiMain

writeIP :: String -> IO ()
writeIP ip = do
    handle <- openFile "ipaddrs.log" AppendMode
    hPutStrLn handle ip
    hClose handle

cgiMain :: CGI CGIResult
cgiMain = do
    addr <- getVarWithDefault "REMOTE_ADDR" "no IP"
    liftIO $ writeIP addr
    showLog

showLog :: CGI CGIResult
showLog = do
    addresses <- liftIO $ readFile "ipaddrs.log"
    output addresses
