import Control.Applicative

data Example = Example String String
               deriving (Show)

withApplicative :: IO Example
withApplicative = Example <$> getLine <*> getLine

withoutApplicative :: IO Example
withoutApplicative = do
    a <- getLine
    b <- getLine
    return $ Example a b
