import Control.Monad.Reader

addOne :: Reader Int Int
addOne = do
    val <- ask
    return $ val + 1

main :: IO ()
main = print $ runReader addOne 10
