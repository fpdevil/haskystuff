import Network.CGI
import System.IO

cgiMain :: CGI CGIResult
cgiMain = getInput "action" >>= processAction

main :: IO ()
main = initMessagesFile >> runCGI cgiMain

initMessagesFile :: IO ()
initMessagesFile = writeMessage ""

processAction :: Maybe String -> CGI CGIResult
processAction (Just "post") = do
    mmessage <- getInput "message"
    case mmessage of
        Nothing      -> output "Form did not contain all required values"
        Just message -> do
            liftIO $ writeMessage message
            redirect "cgi5"

processAction _ = do
    messages <- liftIO readMessages >>= return . messagesHtml
    output $ messages ++ formHtml

messagesHtml :: [String] -> String
messagesHtml ms = concatMap (\m -> "<p>" ++ m ++ "</p>")  ms

formHtml :: String
formHtml = unlines
            [ "<form action=?action=post method=post>"
            , "<input type=text name=message>"
            , "<input type=submit value=Send>"
            ]

readMessages :: IO [String]
readMessages = readFile "messages.dat" >>= return . lines

writeMessage :: String -> IO ()
writeMessage m = withFile "messages.dat" AppendMode $ (\handle -> hPutStrLn handle m)
