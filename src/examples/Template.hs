module Template
        ( loadTemplate
        , loadNamedTemplates
        )
        where

import Text.StringTemplate
import System.FilePath

loadTemplate :: String -> IO (StringTemplate String)
loadTemplate file = readFile file >>= return . newSTMP

loadNamedTemplates :: [String] -> IO [(String, StringTemplate String)]
loadNamedTemplates files = mapM loadTemplate files >>= return . zip plainNames
    where
        plainNames = map (dropExtension . takeFileName) files
