main :: IO ()
main = (return $ User "test" "pass")
        >>= (\u -> (putStrLn $ userName u) >> return u)
        >>= (\u -> putStrLn $ userPassword u)
