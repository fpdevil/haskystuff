import Control.Monad.Writer

doSomething :: Writer [String] Int
doSomething = do
    let x = 1
    tell ["set x to 1"]
    let y = x + 10
    tell ["y is " ++ (show y)]
    return y
