{-# LANGUAGE OverloadedStrings #-}
module AuthBlaze where

import Text.Blaze.Html5
import Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Internal
import Control.Monad
import Data.Maybe

authForm :: String -> Maybe String -> Html
authForm targetPage mmsg = do
    h1 "Login form"
    when (isJust mmsg) (p $ toHtml $ fromJust mmsg)
    H.form ! method "POST" ! action (stringValue targetPage) $ do
        input ! type_ "text" ! name "login" ! placeholder "Username"
        input ! type_ "password" ! name "password" ! placeholder "Password"
        input ! type_ "submit" ! value "Login"
