{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import Network.Wai.Handler.Warp (run)
import Network.HTTP.Types
import Control.Monad.IO.Class
import Data.Text (Text, intercalate, dropWhileEnd)
import qualified Data.ByteString.Lazy.Char8 as BSL
import Text.StringTemplate
import Template

-- Added
import TemplateBlaze
import Text.Blaze.Html.Renderer.Utf8
--

data Route = Route { routeUrl :: Text
                   , routeHandler :: Application
                   }

lookupHandler :: [Route] -> Text -> Maybe Application
lookupHandler routes path = lookup (dropWhileEnd (== '/') path) routePairs
    where
        routePairs = map (\(Route url handler) -> (url, handler)) routes

dispatch :: [Route] -> Application
dispatch routes req = case lookupHandler routes $ intercalate "/" $ pathInfo req of
                        Nothing  -> return $ responseLBS status404 [] "Not Found"
                        Just app -> app req

-- Changed
routes = [ Route "" homeHandler
         , Route "ip" ipHandler
         , Route "about" aboutHandler
         ]
--

homeHandler :: Application
homeHandler req = do
    tpl <- liftIO $ loadTemplate "home.st"
    let html = render $ setAttribute "ip" (show $ remoteHost req) tpl
    return $ responseLBS status200 [] $ BSL.pack html

-- Added
ipHandler :: Application
ipHandler req = do
    return $ responseLBS status200 [] $ renderHtml $ layout $ ipPage (remoteHost req)

aboutHandler :: Application
aboutHandler _ = do
    return $ responseLBS status200 [] $ renderHtml $ layout aboutPage
--

main :: IO ()
main = run 8010 $ dispatch routes
