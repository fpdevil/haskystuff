{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp
import Network.HTTP.Types
import Control.Monad.IO.Class
import Data.Text (Text, intercalate, dropWhileEnd)
import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as BSL
import Text.StringTemplate
import Template
import TemplateBlaze
import Text.Blaze.Html.Renderer.Utf8
import Database.HDBC
import Database.HDBC.Sqlite3
import Data.Maybe
import Control.Monad.Reader
import Data.Monoid
import Control.Monad.Writer
import Web.Cookie
import Network.HTTP.Types.Header
import Blaze.ByteString.Builder
import qualified Data.CaseInsensitive as CI
import qualified Data.ByteString.Char8 as BS
import Crypto.Random
import Data.ByteString.Base64
import Crypto.BCrypt
import Network.Wai.Parse
import Data.Conduit (runResourceT)
import AuthBlaze
import Control.Applicative

data WebappEnv = WebappEnv { envRoutes :: [Route]
                           , envConnection :: Connection
                           }

data ResponseFragment = ResponseFragment { rfHeaders :: [Header]
                                         , rfBody :: Text
                                         }

instance Monoid ResponseFragment where
    mempty = ResponseFragment [] ""
    mappend a b = ResponseFragment (concat $ map rfHeaders [a, b]) (T.concat $ map rfBody [a, b])

type ResponseWriter = WriterT ResponseFragment IO
type WebappMonad = ReaderT WebappEnv ResponseWriter

type Webapp = Request -> WebappMonad Status

runDB :: (Connection -> String -> [SqlValue] -> IO a) -> String -> [SqlValue] -> WebappMonad a
runDB fn query params = do
    conn <- ask >>= return . envConnection
    liftIO $ fn conn query params

writeBody :: Text -> WebappMonad ()
writeBody t = lift $ tell $ ResponseFragment [] t

writeHeaders :: [Header] -> WebappMonad ()
writeHeaders h = lift $ tell $ ResponseFragment h ""

data Route = Route { routeUrl :: Text
                   , routeHandler :: Webapp
                   }

lookupHandler :: [Route] -> Text -> Maybe Webapp
lookupHandler routes path = lookup (dropWhileEnd (== '/') path) routePairs
    where
        routePairs = map (\(Route url handler) -> (url, handler)) routes

dispatch :: [Route] -> Webapp
dispatch routes req = case lookupHandler routes $ intercalate "/" $ pathInfo req of
                        Nothing  -> return status404
                        Just app -> app req

parseCookies :: Request -> CookiesText
parseCookies r = maybe [] parseCookiesText $ lookup hCookie $ requestHeaders r

cookieHeader :: SetCookie -> (HeaderName, BS.ByteString)
cookieHeader sc = (CI.mk "Set-Cookie", toByteString $ renderSetCookie $ sc)

generateSessionID :: IO (Maybe BS.ByteString)
generateSessionID = do
    g <- newGenIO :: IO SystemRandom
    return $ either (const Nothing) (Just . encode . fst) $ genBytes 40 g

data User = User { login :: Text
                 , password :: Text
                 }
                 deriving (Show)

userFromRow :: [(String, SqlValue)] -> Maybe (Int, User)
userFromRow r = (,) <$> uid <*> user
    where uid = fmap fromSql $ lookup "id" r
          login = fmap fromSql $ lookup "login" r
          pass = fmap fromSql $ lookup "password" r
          user = User <$> login <*> pass

textToBS :: Text -> BS.ByteString
textToBS = BS.pack . T.unpack

bsToText :: BS.ByteString -> Text
bsToText = T.pack . BS.unpack

hashPassword :: Text -> IO (Maybe Text)
hashPassword pass = do
    mhash <- hashPasswordUsingPolicy slowerBcryptHashingPolicy $ textToBS pass
    return $ fmap bsToText mhash

testPassword :: Text -> Text -> Bool
testPassword pass hash = validatePassword (textToBS hash) (textToBS pass)

runPost :: Request -> ([(BS.ByteString, BS.ByteString)] -> a) -> WebappMonad a
runPost req form = do
    (params, _) <- liftIO $ runResourceT $ parseRequestBody tempFileBackEnd req
    return $ form params

userForm :: [(BS.ByteString, BS.ByteString)] -> Maybe (Text, Text)
userForm params = (,) <$> (fmap bsToText $ lookup "login" params) <*> (fmap bsToText $ lookup "password" params)

queryRow :: Connection -> String -> [SqlValue] -> IO (Maybe [(String, SqlValue)])
queryRow conn sql args = do
    stmt <- prepare conn sql
    execute stmt args
    fetchRowAL stmt

tryAuth :: (Text, Text) -> WebappMonad (Maybe (Int, User))
tryAuth (login, pass) = do
        mrow <- runDB queryRow "SELECT id, login, password FROM users WHERE login = ?" [toSql login] 
        return $ mfilter ((testPassword pass) . password . snd) (join $ fmap userFromRow mrow)

routes = [ Route "" homeHandler
         , Route "ip" ipHandler
         , Route "about" aboutHandler
         , Route "auth/login" loginHandler
         ]

homeHandler :: Webapp
homeHandler req = do
    result <- runDB quickQuery' "SELECT * FROM users" []
    -- Do something with result

    tpl <- liftIO $ loadTemplate "home.st"
    let html = render $ setAttribute "ip" (show $ remoteHost req) tpl

    writeBody $ T.pack html
    writeHeaders [cookieHeader $ def { setCookieName = "test", setCookieValue = "world" }]
    return status200

ipHandler :: Webapp
ipHandler req = do
    writeBody $ T.pack $ BSL.unpack $ renderHtml $ layout $ ipPage $ remoteHost req
    return status200

aboutHandler :: Webapp
aboutHandler _ = do
    writeBody $ T.pack $ BSL.unpack $ renderHtml $ layout aboutPage
    return status200

loginHandler :: Webapp
loginHandler req =
    let writeForm m = writeBody $ T.pack $ BSL.unpack $ renderHtml $ layout $ authForm "/auth/login" m
    in if (requestMethod req) /= methodPost
         then do
            writeForm Nothing
            return status200
         else do
            muser <- runPost req userForm >>= return . (fmap tryAuth)
            case muser of
              Just user -> do 
                            writeBody "Yay"
                            return status200
              Nothing -> do
                            writeBody "Nay"
                            return status200

main :: IO ()
main = do
    c <- connectSqlite3 "test.db"
    initDB c
    let ctx = WebappEnv routes c
    Warp.run 8010 $ runWebapp ctx

runWebapp :: WebappEnv -> Application
runWebapp context req = do
    (status, frag) <- liftIO $ runWriterT (runReaderT (dispatch (envRoutes context) req) context)
    return $ responseLBS status (rfHeaders frag) (BSL.pack $ T.unpack $ rfBody frag)

tableDefs :: [(String, String)]
tableDefs = [ ("users", "CREATE TABLE users "
                     ++ "(id INT NOT NULL PRIMARY KEY,"
                     ++ "login VARCHAR(100) NOT NULL,"
                     ++ "password VARCHAR(100) NOT NULL)")
            , ("sessions", "CREATE TABLE sessions (id VARCHAR(100) NOT NULL, user_id INT NOT NULL)")
            ]

initDB :: Connection -> IO ()
initDB conn = do
        tables <- quickQuery' conn "SELECT name FROM sqlite_master WHERE type = 'table'" []
        let tableNames = map getName tables
        forM_ tableDefs (\(tblName, tblSql) -> do
            case elem tblName tableNames of
              True -> return ()
              False -> do run conn tblSql []
                          return ()
          )
    where 
        getName :: [SqlValue] -> String
        getName [name] = fromJust $ fromSql name
