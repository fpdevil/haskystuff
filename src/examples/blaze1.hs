{-# LANGUAGE OverloadedStrings #-}
import Text.Blaze.Html5
import Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

page :: Html
page = docTypeHtml $ body $ do
    h1 "Blaze page"
    p "Hello"
