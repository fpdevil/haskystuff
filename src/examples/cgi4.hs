import Network.CGI
import System.IO

main :: IO ()
main = runCGI cgiMain

writeIP :: String -> IO ()
writeIP ip = do
    handle <- openFile "ipaddrs.log" AppendMode
    hPutStrLn handle ip
    hClose handle

cgiMain :: CGI CGIResult
cgiMain = do
    mAddr <- getVar "REMOTE_ADDR"
    case mAddr of
      Nothing -> do
          liftIO $ writeIP "no IP"
          showLog
      Just ip -> do
          liftIO $ writeIP ip
          showLog

showLog :: CGI CGIResult
showLog = do
    addresses <- liftIO $ readFile "ipaddrs.log"
    output addresses
