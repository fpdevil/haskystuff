import Network.CGI
import Data.Time

cgiMain :: CGI CGIResult
cgiMain = do
    time <- liftIO getCurrentTime
    output (show time)

main :: IO ()
main = runCGI cgiMain

