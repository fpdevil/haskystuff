type ParsedJSON = [(String, [(Int, String)])]

parsedJson :: ParsedJSON
parsedJson = [ ("results",
                    [ (1, "Han")
                    , (2, "Luke")
                    , (3, "Jar-jar")
                    ])
             ]

findUserName :: ParsedJSON -> Int -> Maybe String
findUserName json id = do
    case lookup "results" json of
      Nothing      -> Nothing
      Just results -> lookup id results
