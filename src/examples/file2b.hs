multiply :: Integer -> Integer -> Integer
multiply a b = a * b

main :: IO ()
main = putStrLn (show (multiply 5 5))
