{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp
import Network.HTTP.Types
import Control.Monad.IO.Class
import Data.Text (Text, intercalate, dropWhileEnd)
import qualified Data.ByteString.Lazy.Char8 as BSL
import Text.StringTemplate
import Template
import TemplateBlaze
import Text.Blaze.Html.Renderer.Utf8
import Database.HDBC
import Database.HDBC.Sqlite3
import Data.Maybe

data Route = Route { routeUrl :: Text
                   , routeHandler :: Connection -> Application
                   }

lookupHandler :: [Route] -> Text -> Maybe (Connection -> Application)
lookupHandler routes path = lookup (dropWhileEnd (== '/') path) routePairs
    where
        routePairs = map (\(Route url handler) -> (url, handler)) routes

dispatch :: [Route] -> Connection -> Application
dispatch routes conn req = case lookupHandler routes $ intercalate "/" $ pathInfo req of
                        Nothing  -> return $ responseLBS status404 [] "Not Found"
                        Just app -> app conn req

routes = [ Route "" homeHandler
         , Route "ip" ipHandler
         , Route "about" aboutHandler
         ]

homeHandler :: Connection -> Application
homeHandler conn req = do
    tpl <- liftIO $ loadTemplate "home.st"
    let html = render $ setAttribute "ip" (show $ remoteHost req) tpl
    return $ responseLBS status200 [] $ BSL.pack html

ipHandler :: Connection -> Application
ipHandler conn req = do
    return $ responseLBS status200 [] $ renderHtml $ layout $ ipPage (remoteHost req)

aboutHandler :: Connection -> Application
aboutHandler _ _ = do
    return $ responseLBS status200 [] $ renderHtml $ layout aboutPage

main :: IO ()
main = do
    c <- connectSqlite3 "test.db"
    initDB c
    Warp.run 8010 $ dispatch routes c

initDB :: Connection -> IO ()
initDB conn = do
        tables <- quickQuery' conn "SELECT name FROM sqlite_master WHERE type = 'table'" []
        let tableNames = map getName tables
        case elem "users" tableNames of
          True -> return ()
          False -> do run conn ("CREATE TABLE users "
                            ++ "(id INT NOT NULL PRIMARY KEY,"
                            ++ "login VARCHAR(100) NOT NULL,"
                            ++ "password VARCHAR(100) NOT NULL)") []
                      return ()
    where 
        getName :: [SqlValue] -> String
        getName [name] = fromJust $ fromSql name
