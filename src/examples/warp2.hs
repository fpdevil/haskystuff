{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import Data.Text (Text)

data Route = Route { routeUrl :: Text
                   , routeHandler :: Application
                   }

lookupHandler :: [Route] -> Text -> Maybe Application
lookupHandler routes path = lookup path routePairs
    where
        routePairs = map (\(Route url handler) -> (url, handler)) routes
