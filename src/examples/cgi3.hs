import Network.CGI

main :: IO ()
main = runCGI cgiMain

cgiMain :: CGI CGIResult
cgiMain = do
    action <- getInput "page"
    processAction action

processAction :: Maybe String -> CGI CGIResult
processAction (Just "foo") = output $ "<p>Foo page</p>" ++ links
processAction (Just "bar") = output $ "<p>Bar page</p>" ++ links
processAction _ = output $ "<p>Home page</p>" ++ links

links :: String
links = unlines
        [ "<p><a href=\"?page=foo\">Go to Page 2</a></p>"
        , "<p><a href=\"?page=bar\">Go to Page 3</a></p>" 
        ]
