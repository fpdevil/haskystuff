{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp
import Network.HTTP.Types
import Control.Monad.IO.Class
import Data.Text (Text, intercalate, dropWhileEnd)
import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as BSL
import Text.StringTemplate
import Template
import TemplateBlaze
import Text.Blaze.Html.Renderer.Utf8
import Database.HDBC
import Database.HDBC.Sqlite3
import Data.Maybe
import Control.Monad.Reader
import Data.Monoid
import Control.Monad.Writer

data WebappEnv = WebappEnv { envRoutes :: [Route]
                           , envConnection :: Connection
                           }

data ResponseFragment = ResponseFragment { rfHeaders :: [Header]
                                         , rfBody :: Text
                                         }

instance Monoid ResponseFragment where
    mempty = ResponseFragment [] ""
    mappend a b = ResponseFragment (concat $ map rfHeaders [a, b]) (T.concat $ map rfBody [a, b])

type ResponseWriter = WriterT ResponseFragment IO
type WebappMonad = ReaderT WebappEnv ResponseWriter

type Webapp = Request -> WebappMonad Status

runDB :: (Connection -> String -> [SqlValue] -> IO a) -> String -> [SqlValue] -> WebappMonad a
runDB fn query params = do
    conn <- ask >>= return . envConnection
    liftIO $ fn conn query params

writeBody :: Text -> WebappMonad ()
writeBody t = lift $ tell $ ResponseFragment [] t

writeHeaders :: [Header] -> WebappMonad ()
writeHeaders h = lift $ tell $ ResponseFragment h ""

data Route = Route { routeUrl :: Text
                   , routeHandler :: Webapp
                   }

lookupHandler :: [Route] -> Text -> Maybe Webapp
lookupHandler routes path = lookup (dropWhileEnd (== '/') path) routePairs
    where
        routePairs = map (\(Route url handler) -> (url, handler)) routes

dispatch :: [Route] -> Webapp
dispatch routes req = case lookupHandler routes $ intercalate "/" $ pathInfo req of
                        Nothing  -> return status404
                        Just app -> app req

routes = [ Route "" homeHandler
         , Route "ip" ipHandler
         , Route "about" aboutHandler
         ]

homeHandler :: Webapp
homeHandler req = do
    result <- runDB quickQuery' "SELECT * FROM users" []
    -- Do something with result

    tpl <- liftIO $ loadTemplate "home.st"
    let html = render $ setAttribute "ip" (show $ remoteHost req) tpl

    writeBody $ T.pack html
    return status200

ipHandler :: Webapp
ipHandler req = do
    writeBody $ T.pack $ BSL.unpack $ renderHtml $ layout $ ipPage $ remoteHost req
    return status200

aboutHandler :: Webapp
aboutHandler _ = do
    writeBody $ T.pack $ BSL.unpack $ renderHtml $ layout aboutPage
    return status200

main :: IO ()
main = do
    c <- connectSqlite3 "test.db"
    initDB c
    let ctx = WebappEnv routes c
    Warp.run 8010 $ runWebapp ctx

runWebapp :: WebappEnv -> Application
runWebapp context req = do
    (status, frag) <- liftIO $ runWriterT (runReaderT (dispatch (envRoutes context) req) context)
    return $ responseLBS status (rfHeaders frag) (BSL.pack $ T.unpack $ rfBody frag)


initDB :: Connection -> IO ()
initDB conn = do
        tables <- quickQuery' conn "SELECT name FROM sqlite_master WHERE type = 'table'" []
        let tableNames = map getName tables
        case elem "users" tableNames of
          True -> return ()
          False -> do run conn ("CREATE TABLE users "
                            ++ "(id INT NOT NULL PRIMARY KEY,"
                            ++ "login VARCHAR(100) NOT NULL,"
                            ++ "password VARCHAR(100) NOT NULL)") []
                      return ()
    where 
        getName :: [SqlValue] -> String
        getName [name] = fromJust $ fromSql name
