import Control.Monad.Reader
import Control.Monad.IO.Class (liftIO)

printEnv :: ReaderT Int IO ()
printEnv = do
    val <- ask
    liftIO $ print val

main :: IO ()
main = runReaderT printEnv 10
