{-# LANGUAGE OverloadedStrings #-}
module TemplateBlaze 
        ( layout
        , ipPage
        , aboutPage
        )
        where

import Text.Blaze.Html5
import Text.Blaze.Html5.Attributes
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Network.Socket (SockAddr)

layout :: Html -> Html
layout bodyFn = docTypeHtml $ do
    H.head $ H.title "Sample"
    body bodyFn

aboutPage :: Html
aboutPage = do
    h1 "This is the about page"
    p "This is about page content"

ipPage :: SockAddr -> Html
ipPage addr = do
    h1 "This page shows your IP"
    H.div ! A.style "border: 2px solid black" $ do
        p $ toHtml $ "Your IP address is:" ++ (show addr)
