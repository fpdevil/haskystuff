{-# LANGUAGE OverloadedStrings #-}
import Network.Wai
import Network.Wai.Handler.Warp (run)
import Network.HTTP.Types
import Data.Text (Text, intercalate)

data Route = Route { routeUrl :: Text
                   , routeHandler :: Application
                   }

lookupHandler :: [Route] -> Text -> Maybe Application
lookupHandler routes path = lookup path routePairs
    where
        routePairs = map (\(Route url handler) -> (url, handler)) routes

dispatch :: [Route] -> Application
dispatch routes req = case lookupHandler routes $ intercalate "/" $ pathInfo req of
                        Nothing  -> return $ responseLBS status404 [] "Not Found"
                        Just app -> app req

routes = [ Route "foo" (\_ -> return $ responseLBS status200 [] "foo")
         , Route "bar" (\_ -> return $ responseLBS status200 [] "bar")
         ]

main :: IO ()
main = run 8010 $ dispatch routes
