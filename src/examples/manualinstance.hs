data Doctype = HTML5 | HTML4 | XHTML

instance Show Doctype where
    show HTML5 = "HTML5"
    show HTML4 = "HTML4"
    show XHTML = "XHTML"

instance Eq Doctype where
    HTML5 == HTML5 = True
    HTML4 == HTML4 = True
    XHTML == XHTML = True
    _ == _ = False

